<?php

class router{
    public static function init(){
        $route  = explode(DIRECTORY_SEPARATOR,$_SERVER['REQUEST_URI']);
        include __DIR__.DIRECTORY_SEPARATOR.$route[1].DIRECTORY_SEPARATOR.'controller'.DIRECTORY_SEPARATOR.'api.php';
        $args   = array_slice($route, 4);
        api::$route[3]($args);
    }
}

