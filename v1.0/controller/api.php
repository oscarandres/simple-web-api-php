<?php
require __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.'user.php';

class api{
    public static function __callStatic($name, $arguments) {
        self::$name($arguments);
    }

    public static function get($args){
        $user   = new user();
        echo json_encode($user->get($args[0]));
    }
    
    public static function delete($args){
        $user   = new user();
        echo json_encode($user->delete($args[0]));
    }

    public static function change($args){
        $user   = new user();
        echo json_encode($user->change($args[0],$args[1],$args[2],$args[3]));
    }
}

